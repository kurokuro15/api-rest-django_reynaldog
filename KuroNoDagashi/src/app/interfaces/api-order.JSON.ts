export interface Order {
  id?: number;
  date?: string;
  total?: number;
  details?: string;
  iva?: number;
  desc?: number;
  cart?: number;
  client?: number;
}

export interface Cart {
  id?: number;
  order?: number;
  product?: number;
  quantity?: number;
}
