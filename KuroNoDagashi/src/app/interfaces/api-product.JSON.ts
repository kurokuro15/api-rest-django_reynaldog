export interface Product {
  category: number;
  description: string;
  url: string;
  lot: number;
  name: string;
  id: number;
  price: number;
}




export interface Category {
  id: number;
  name: string;
  description: string;
}

