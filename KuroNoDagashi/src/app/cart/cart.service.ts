import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { Cart, Order } from '../interfaces/api-order.JSON';
import { Product } from '../interfaces/api-product.JSON';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  productList: any[];
  product: Product;
  products: Product[];
  cart: Cart;
  Order: Order;

  constructor(private activatedRoute: ActivatedRoute, private api:ApiService) { }

  activatedRoute.params.subscribe((prm) => {
    console.log(`El id es: ${prm['id']}`);
    let id = prm['id'];
    api.getProduct(id).subscribe(
      (data) => {
        this.product = data;
        console.log(this.product);
        this.products = [];
        this.products.push(this.product);
      },
      (err) => {
        console.log('Error, no se realizó la consulta.');
      }
    );
  });
}
