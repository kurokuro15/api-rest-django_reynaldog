import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { ApiService } from '../api.service';
import { Cart, Order } from '../interfaces/api-order.JSON';
import { Product } from '../interfaces/api-product.JSON';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  fatrash = faTrash;
  productList: any[];
  product: Product;
  products: Product[];
  cart: Cart;
  Order: Order;

  constructor(private activatedRoute: ActivatedRoute, private api:ApiService) {
    activatedRoute.params.subscribe((prm) => {
      console.log(`El id es: ${prm['id']}`);
      let id = prm['id'];
      api.getProduct(id).subscribe(
        (data) => {
          this.product = data;
          console.log(this.product);
          this.products = [];
          this.products.push(this.product);
        },
        (err) => {
          console.log('Error, no se realizó la consulta.');
        }
      );
    });
  }

  ngOnInit(): void {
  }

}
