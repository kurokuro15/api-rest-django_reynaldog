import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class ApiService {
  BASE_URL = 'http://127.0.0.1:8000/';
  Product_URL = this.BASE_URL + 'Producto/';
  Category_URL = this.BASE_URL + 'Categoria/';
  Order_URL = this.BASE_URL + 'Pedido/';
  Cart_URL = this.BASE_URL + 'Carrito/';
  Header = new HttpHeaders({ 'Content-Type': 'application/json' });
  Headerm = new HttpHeaders({ 'Content-Type': 'multipart/form-data' });
  Category_Filter = `?category=`;

  constructor(private http: HttpClient) {}

  getCategory(): Observable<any> {
    return this.http.get(this.BASE_URL + 'Categoria/', {
      headers: this.Header,
    });
  }

  getAllProducts(filter: any = null, parameter: any = null): Observable<any> {
    if (filter == null) {
      return this.http.get(this.Product_URL, {
        headers: this.Header,
      });
    } else {
      return this.http.get(this.Product_URL + filter + parameter, {
        headers: this.Header,
      });
    }
  }

  getProduct(id: any): Observable<any> {
    return this.http.get(this.Product_URL + id + '/', {
      headers: this.Header,
    });
  }

  addProduct(product: any): Observable<any> {
    return this.http.post(this.Product_URL, product);
  }

  updateProduct(product: any): Observable<any> {
    console.log(product);
    return this.http.patch(this.Product_URL + product.get('id') + '/', product);
  }

  deleteProduct(id: any): Observable<any> {
    return this.http.delete(this.Product_URL + id);
  }



  getAllCart(): Observable<any> {
    return this.http.get(this.Cart_URL);
  }

  createCart(cart: any):Observable<any> {
    return this.http.post(this.Product_URL, cart);
  }
  updateCart(cartProduct: any): Observable<any> {
    return this.http.patch(this.Cart_URL + cartProduct.get('id') + '/', cartProduct);
  }

  deleteCart(cart: any): Observable<any> {
    return this.http.delete(this.Cart_URL + cart);
  }



  getAllOrder(): Observable<any> {
    return this.http.get(this.Order_URL);
  }
  createOrder(order: any):Observable<any> {
    return this.http.post(this.Order_URL, order);
  }

  updateOrder(order: any):Observable<any>{
    return this.http.patch(this.Order_URL + order.get('id') + '/', order);
  }

  deleteOrder(id: any):Observable<any>{
    return this.http.delete(this.Order_URL + id);
  }
}
