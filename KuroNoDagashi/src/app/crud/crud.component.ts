import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Product } from '../interfaces/api-product.JSON';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.css'],
})
export class CrudComponent implements OnInit {
  products: Product[];

  constructor(public api: ApiService, private router:Router) {
    this.api.getAllProducts().subscribe(
      (data) => {
        this.products = data;
      },
      (err) => {
        console.log('Error.');
      }
    );
  }

  ngOnInit(): void {


  }
  getId(id:number) {
    this.router.navigate(['/detalleProducto', id]);
    console.log(`este es el id enviado: ${id}`);
  }
}
