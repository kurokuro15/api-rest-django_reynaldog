import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Category, Product } from '../interfaces/api-product.JSON';
import { faUndo } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.component.html',
  styleUrls: ['./detail-product.component.css'],
})
export class DetailProductComponent implements OnInit {
  product: Product;
  categories: Category[];
  empaque: string;
  faundo = faUndo;

  constructor(
    private activatedRoute: ActivatedRoute,
    public api: ApiService,
    private location: Location,
    private router: Router
  ) {
    activatedRoute.params.subscribe((prm) => {
      console.log(`El id es: ${prm['id']}`);
      let id = prm['id'];
      api.getProduct(id).subscribe(
        (data) => {
          this.product = data;
          console.log(this.product);
          if (this.product.category == 4) {
            this.empaque = 'Combo de Dulces';
          } else {
            this.empaque = 'Empaque Individual';
          }
        },
        (err) => {
          console.log('Error, no se realizó la consulta.');
        }
      );
      api.getCategory().subscribe(
        (data) => {
          this.categories = data;
          console.log(this.categories);
        },
        (err) => {
          console.log('Error, no se realizó la consulta.');
        }
      );
    });
  }
  ngOnInit(): void {}
  
  goBack(): void {
    this.location.back();
  }

  defCategory(id) {
    let cat = this.categories[id - 1];
    return cat.name;
  }

  goCart(id): void {
    this.router.navigate(['/cart', id]);
    console.log(`este es el id enviado: ${id}`);
  }
}
