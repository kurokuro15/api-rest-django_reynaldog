import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './Home/Home.Component';
import { ShopComponent } from './shop/shop.component';
import { DulcesComponent } from './dulces/dulces.component';
import { ContactoComponent } from './contacto/contacto.component';
import { CartComponent } from './cart/cart.component';

import { DetailProductComponent } from './detail-product/detail-product.component';
import { CrudComponent } from './crud/crud.component';
import { CrudDetailComponent } from './crud-detail/crud-detail.component';
import { ProductForm } from './productform/productform.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'shop', component: ShopComponent },
  { path: 'dulces', component: DulcesComponent },
  { path: 'contacto', component: ContactoComponent},
  { path: 'cart/:id', component: CartComponent},
  { path: 'cart/', component: CartComponent},
  { path: 'producto/:id', component: DetailProductComponent},
  { path: 'admin', component: CrudComponent},
  { path: 'detalleProducto/:id', component: CrudDetailComponent},
  { path: 'crearProducto', component: ProductForm}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
