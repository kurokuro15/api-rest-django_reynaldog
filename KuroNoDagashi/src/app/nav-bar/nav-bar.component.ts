import { Component, OnInit } from '@angular/core';
import { RouterLinkActive } from '@angular/router';
import { faRocket } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  routerLinkActive : RouterLinkActive;
  fatrash = faRocket;
  constructor() { }

  ngOnInit(): void {
  }

}
