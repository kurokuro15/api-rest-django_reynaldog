import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './Home/Home.Component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ShopComponent } from './shop/shop.component';
import { DulcesComponent } from './dulces/dulces.component';
import { ContactoComponent } from './contacto/contacto.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CarouselComponent } from './carousel/carousel.component';
import { FooterComponent } from './footer/footer.component';
import { CartComponent } from './cart/cart.component';

import { HttpClientModule } from "@angular/common/http";


import { DetailProductComponent } from './detail-product/detail-product.component';
import { ApiService } from './api.service';
import { CrudComponent } from './crud/crud.component';
import { CrudDetailComponent } from './crud-detail/crud-detail.component';
import { ProductForm } from './productform/productform.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HomeComponent,
    ShopComponent,
    DulcesComponent,
    ContactoComponent,
    CarouselComponent,
    FooterComponent,
    CartComponent,
    DetailProductComponent,
    CrudComponent,
    CrudDetailComponent,
    ProductForm
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FontAwesomeModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
