import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Category, Product } from '../interfaces/api-product.JSON';

@Component({
  selector: 'app-dulces',
  templateUrl: './dulces.component.html',
  styleUrls: ['./dulces.component.css'],
})
export class DulcesComponent implements OnInit {
  products: Product[];
  categories: Category[];

  constructor(private api: ApiService, private router: Router) {
    this.api.getAllProducts().subscribe(
      (data) => {
        this.products = data;
      },
      (err) => {
        console.log(err);
      }
    );
    this.api.getCategory().subscribe(
      (data) => {
        this.categories = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }
  getId(id: number) {
    this.router.navigate(['/producto', id]);
    console.log(`este es el id enviado: ${id}`);
  }
  ngOnInit(): void {}
}
