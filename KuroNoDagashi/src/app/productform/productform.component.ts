import { Component, OnInit, Input } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';
import { Location } from '@angular/common';
import { Product, Category } from '../interfaces/api-product.JSON';
import { faThList } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-productForm',
  templateUrl: './productform.component.html',
  styleUrls: ['./productform.component.css'],
})
export class ProductForm implements OnInit {
  @Input() productDetailed: Product;

  fn: any;
  product!: Product;
  selectedFile!: File;
  selectedCategory!: Category;
  categories: Category[];
  url: any;
  id: any;
  button :string;

  productForm = this.formBuilder.group({
    id: new FormControl(0),
    name: new FormControl(''),
    price: new FormControl(0.0),
    url: new FormControl(),
    description: new FormControl(''),
    lot: new FormControl(0),
    category: new FormControl(0),
  });

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private location: Location,
    private router: Router,
    private postService: ApiService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.apiService.getCategory().subscribe(
      (data) => {
        this.categories = data;
        console.log(this.categories);
      },
      (err) => {
        console.log('Error, no se realizó la consulta.');
      }
    );

    if (this.productDetailed) {
      this.id = `${this.productDetailed.id}`;
      this.button = 'Editar'
      this.productForm = this.formBuilder.group({
        id: new FormControl(this.productDetailed.id),
        name: new FormControl(this.productDetailed.name),
        price: new FormControl(this.productDetailed.price),
        url: new FormControl(null),
        description: new FormControl(this.productDetailed.description),
        lot: new FormControl(this.productDetailed.lot),
        category: new FormControl(this.productDetailed.category),
      });
      this.fn = this.updateProduct;
      console.log(this.productDetailed);
    } else {
      this.button = 'Añadir'
      this.productForm = this.formBuilder.group({
        name: new FormControl(''),
        price: new FormControl(0.0),
        url: new FormControl(null),
        description: new FormControl(''),
        lot: new FormControl(0),
        category: new FormControl(0),
      });
      this.fn = this.addProduct;
    }
  }

  goBack(): void {
    this.location.back();
  }

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];

    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: any) => {
        this.url = event.target.result;
      };

      reader.readAsDataURL(event.target.files[0]);
    }
  }

  addProduct() {
    const formData = new FormData();
    formData.append('name', this.productForm.get('name').value);
    console.log(`name: ${formData.get('name')}`);
    formData.append('price', this.productForm.get('price').value);
    console.log(`price: ${formData.get('price')}`);
    formData.append('lot', this.productForm.get('lot').value);
    console.log(`lot: ${formData.get('lot')}`);
    formData.append('description', this.productForm.get('description').value);
    console.log(`description: ${formData.get('description')}`);
    formData.append('category', this.productForm.get('category').value);
    console.log(`category: ${formData.get('category').toString()}`);
    formData.append('url', this.selectedFile);
    console.log(`url: ${formData.get('url')}`);
    console.log(formData);
    this.apiService.addProduct(formData).subscribe(
      () => this.goBack(),
      (result) => console.log(result)
    );
  }

  updateProduct() {
    const formData = new FormData();
    formData.append('id', this.productForm.get('id').value);
    console.log(formData.get('id'));
    formData.append('name', this.productForm.get('name').value);
    console.log(formData.get('name'));
    formData.append('price', this.productForm.get('price').value);
    console.log(formData.get('price'));
    formData.append('lot', this.productForm.get('lot').value);
    console.log(formData.get('lot'));
    formData.append('description', this.productForm.get('description').value);
    console.log(formData.get('description'));
    formData.append('category', this.productForm.get('category').value);
    console.log(formData.get('category').toString());
    if (this.selectedFile == null) {
      formData.delete('url');
    } else {
      formData.append('url', this.selectedFile);
    }
    console.log(formData.get('url'));
    console.log(formData);
    this.apiService
      .updateProduct(formData)
      .subscribe( () => this.router.navigate(['/admin']),
      (result) => console.log(result));
  }
}
