import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Category, Product } from '../interfaces/api-product.JSON';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css'],
})
export class ShopComponent implements OnInit {
  products: Product[];
  combo: Product[];
  conca: any;
  constructor(private api: ApiService, private router:Router) {
    this.api.getAllProducts(this.api.Category_Filter, '1').subscribe(
      (data) => {
        let concat = data;
        console.log(data);
        this.api.getAllProducts(this.api.Category_Filter, '2').subscribe(
          (data) => {
            this.conca = concat.concat(data);
            console.log(data);
            this.api.getAllProducts(this.api.Category_Filter, '3').subscribe(
              (data) => {
                this.products = this.conca.concat(data);
              },
              (err) => {
                console.log(err);
              }
            );
          },
          (err) => {
            console.log(err);
          }
        );
      },
      (err) => {
        console.log(err);
      }
    );

    this.api.getAllProducts(this.api.Category_Filter, '4').subscribe(
      (data) => {
        this.combo = data;
        console.log(data);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getId(id:number) {
    this.router.navigate(['/producto', id]);
    console.log(`este es el id enviado: ${id}`);
  }
  ngOnInit(): void {}
}
