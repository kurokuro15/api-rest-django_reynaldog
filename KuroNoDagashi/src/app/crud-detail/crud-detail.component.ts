import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { Product, Category } from '../interfaces/api-product.JSON';
import { Location } from '@angular/common';

@Component({
  selector: 'app-crud-detail',
  templateUrl: './crud-detail.component.html',
  styleUrls: ['./crud-detail.component.css'],
})
export class CrudDetailComponent implements OnInit {
  product: Product;
  categories: Category[];

  constructor(private activatedRoute: ActivatedRoute, public api: ApiService, private location: Location) {
    activatedRoute.params.subscribe((prm) => {
      console.log(`El id es: ${prm['id']}`);
      let id = prm['id'];
      api.getProduct(id).subscribe(
        (data) => {
          this.product = data;
          console.log(this.product);
        },
        (err) => {
          console.log('Error, no se realizó la consulta.');
        })
        api.getCategory().subscribe(
          (data) => {
            this.categories = data;
            console.log(this.categories);
          },
          (err) => {
            console.log('Error, no se realizó la consulta.');
          }
      );
    });
  }
  buttonTitle:string = "Hide";
  visible:boolean = false;
  showhideUtility(){
   this.visible = this.visible?false:true;
   this.buttonTitle = this.visible?"Hide":"Show";
 }

  ngOnInit(): void {}

  goBack(): void {
    this.location.back();
  }

  delProduct(id) {
    this.api.deleteProduct(id).subscribe(
      (data) => {

        this.product = data ();
        this.goBack();
        console.log(this.product);
      },
      (err) => {
        console.log(err);
      }
    )
  }

    defCategory(id) {
      let cat = this.categories[id-1];
      return cat.name;
    }
}

