import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Category, Product } from '../interfaces/api-product.JSON';
@Component({
  selector: 'app-Home',
  templateUrl: './Home.Component.html',
  styleUrls: ['./Home.Component.css'],
})
export class HomeComponent {
  products: Product[];
  categories: Category[];

  constructor(private api: ApiService, private router: Router) {
    this.api.getAllProducts().subscribe(
      (data) => {
        this.products = data;
        console.log(this.products);
      },
      (err) => {
        console.log(err);
      }
    );
    this.api.getCategory().subscribe(
      (data) => {
        this.categories = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }
  getId(id: number) {
    this.router.navigate(['/producto', id]);
    console.log(`este es el id enviado: ${id}`);
  }
  ngOnInit(): void {  }
}
