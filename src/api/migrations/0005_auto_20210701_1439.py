# Generated by Django 3.2.4 on 2021-07-01 18:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_alter_imagen_url'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='producto',
            name='url_img',
        ),
        migrations.AddField(
            model_name='producto',
            name='url',
            field=models.ImageField(null=True, upload_to='', verbose_name='image'),
        ),
        migrations.DeleteModel(
            name='Imagen',
        ),
    ]
