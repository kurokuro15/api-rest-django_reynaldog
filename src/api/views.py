
from rest_framework import viewsets, permissions
from django_filters.rest_framework import DjangoFilterBackend
from api.serializers import CarritoSerializer, CategoriaSerializer, ProductoSerializer, PedidoSerializer, ClienteSerializer, UserSerializer
from api.models import Carrito, Producto, Pedido, Cliente, User, Categoria
#from rest_flex_fields.views import FlexFieldsModelViewSet

# Create your views here.

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class CategoriaViewSet(viewsets.ModelViewSet):
    """
    Automaticamente provee las acciones: `list`,`create`, `retrieve`, `update` and `destroy`. 
    """
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerializer


class ProductoViewSet(viewsets.ModelViewSet):
    """
    Automaticamente provee las acciones: `list`,`create`, `retrieve`, `update` and `destroy`. 
    """
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer
    permission_classes = [permissions.AllowAny]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['category']
    ordering_fields = '__all__'
    

class PedidoViewSet(viewsets.ModelViewSet):
    """
    Automaticamente provee las acciones: `list`,`create`, `retrieve`, `update` and `destroy`. 
    """
    queryset = Pedido.objects.all()
    serializer_class = PedidoSerializer


class ClienteViewSet(viewsets.ModelViewSet): 
    """
    Automaticamente provee las acciones: `list`,`create`, `retrieve`, `update` and `destroy`. 
    """
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer


# class ImageViewSet(FlexFieldsModelViewSet):
#     """
#     Automaticamente provee las acciones: `list`,`create`, `retrieve`, `update` and `destroy`. 
#     """
#     queryset = Imagen.objects.all()
#     serializer_class = ImageSerializer

class CarritoViewSet(viewsets.ModelViewSet):
    """
    Automaticamente provee las acciones: `list`,`create`, `retrieve`, `update` and `destroy`. 
    """
    queryset = Carrito.objects.all()
    serializer_class = CarritoSerializer
