from django.db import models
from django.db.models.fields.files import ImageField
from versatileimagefield.fields import VersatileImageField, PPOIField
from django.contrib.auth.models import User
# Create your models here.


class Cliente(models.Model):
    """Modelo de base de dato para los Clientes asociados a su respectivo Usuario."""
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    ci = models.IntegerField(blank=False, null=False)
    address = models.TextField(blank=False, null=False)
    telf = models.IntegerField(blank=False, null=False)

    class Meta:
        """Ordenado de menor a mayor por usuario """
        ordering = ['-user']
        verbose_name = "Cliente"
        verbose_name_plural = "Clientes"

    def __str__(self) -> str:
        return self.user.username


class Categoria(models.Model):
    """Modelo de base de dato para las Categorías de los Productos."""
    name = models.CharField(max_length=75, blank=False, null=False, unique= True)
    description = models.CharField(max_length=150, blank=True, null=True)
    class Meta:
        """Ordenado de menor a mayor ( a, b, c ...  x, y, z.) """
        ordering = ['id']
        verbose_name = "Categoría"
        verbose_name_plural = "Categorías"

    def __str__(self) -> str:
        return self.name

class Producto(models.Model):
    """Modelo de base de dato para los Productos."""
    created_at = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=75, blank=False, null=False)
    price = models.FloatField(blank=False, default='null')
    lot = models.IntegerField(blank=False, null=False, default=0)
    description = models.TextField(blank=False, null=False)
    category = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    url = models.ImageField('image', blank = False, null = True)
    

    class Meta:
        """Ordenado de menor a mayor ( a, b, c ...  x, y, z.) """
        ordering = ['-created_at']
        verbose_name = "Producto"
        verbose_name_plural = "Productos"

    def __str__(self) -> str:
        return self.name


class Pedido(models.Model):
    date = models.DateTimeField(auto_now_add= True)
    total = models.FloatField(blank=False, default='null')
    details = models.TextField(blank=True, null=True)
    iva = models.FloatField(blank=False, null=False)
    desc = models.FloatField(blank=True, null=True)
    cart = models.ManyToManyField(Producto, through = 'Carrito', through_fields=('order','product'),)
    client = models.ManyToManyField(Cliente)
    class Meta:
        """Ordenado de más reciente a más antiguo """
        ordering = ['-date']
        verbose_name = "Pedido"
        verbose_name_plural = "Pedidos"

    def __str__(self) -> str:
        return (self.pk + " " + self.date)

class Carrito(models.Model):
    order = models.ForeignKey(Pedido, on_delete = models.CASCADE)
    product = models.ForeignKey(Producto, on_delete= models.CASCADE)
    quantity = models.IntegerField(blank=False, null=True, default=0)
    created_at = models.DateTimeField(auto_now_add = True)
    
class Venta(models.Model):
    payment = models.CharField(max_length=45, blank=False, null=False)
    timestamp = models.DateTimeField(auto_now_add= True)
    order = models.OneToOneField(Pedido, on_delete=models.CASCADE)
    client = models.ForeignKey(Cliente, on_delete=models.CASCADE)
