from rest_framework import serializers
from api.models import Carrito, Categoria, Cliente, Pedido, Producto #, Imagen
from django.contrib.auth.models import User
# from versatileimagefield.serializers import VersatileImageFieldSerializer
# from rest_flex_fields.serializers import FlexFieldsModelSerializer

class CategoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categoria
        fields = ('id', 'name','description')
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('pk', 'username','password', 'email', 'first_name', 'last_name', 'is_staff', 'is_active', 'groups', 'user_permissions', 'last_login', 'date_joined')



class ClienteSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Cliente
        fields = ('pk', 'user', 'ci', 'address', 'telf')


# class ImageSerializer(FlexFieldsModelSerializer):
#     image = VersatileImageFieldSerializer(
#         sizes= 'product_headshot'
#     )

#     class Meta:
#         model = Imagen
#         fields = '__all__'


class ProductoSerializer(serializers.ModelSerializer):
   # image = ImageSerializer(many=True)
    class Meta:
        model = Producto
        fields = ('id', 'created_at', 'name', 'price', 'lot', 'description', 'category', 'url')


class PedidoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pedido
        fields = '__all__'

class CarritoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Carrito
        fields = '__all__'