from django.contrib import admin

# Register your models here.
from .models import Categoria, Cliente, Producto, Pedido, Carrito
# Register your models here.
admin.site.register(Cliente)
admin.site.register(Producto)
admin.site.register(Pedido)
admin.site.register(Categoria)
admin.site.register(Carrito)