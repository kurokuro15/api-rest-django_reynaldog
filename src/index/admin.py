from django.contrib import admin
from .models import Categoria, Usuario, Cliente, Producto, Pedido, Image
# Register your models here.
admin.site.register(Cliente)
admin.site.register(Producto)
admin.site.register(Pedido)
admin.site.register(Categoria)
admin.site.register(Usuario)
admin.site.register(Image)