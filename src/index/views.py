from django.shortcuts import render
from .models import Producto
from .forms import ProductoForms
from django.views import generic
from django.urls import reverse_lazy


class HomeView(generic.ListView):
    model = Producto
    template_name = 'index.html'



class NewProduct(generic.CreateView):
    model = Producto
    form_class = ProductoForms
    template_name = 'crearProducto.html'
    success_url = reverse_lazy('inicio')


class UpdateProduct(generic.UpdateView):
    model = Producto
    form_class = ProductoForms
    template_name = 'crearProducto.html'
    success_url = reverse_lazy('inicio')


class DeleteProduct(generic.DeleteView):
    model = Producto
    template_name = 'validate.html'
    success_url = reverse_lazy('inicio')
