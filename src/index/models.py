from django.db import models
from versatileimagefield.fields import VersatileImageField, \
    PPOIField
# Create your models here.


class Usuario(models.Model):
    nickname = models.CharField(max_length=45, blank=False, null=False)
    password = models.CharField(max_length=45, blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)


class Cliente(models.Model):
    nombre = models.CharField(max_length=45, blank=False, null=False)
    apellido = models.CharField(max_length=45, blank=False, null=False)
    ci = models.IntegerField(blank=False, null=False)
    telf = models.IntegerField(blank=False, null=False)
    email = models.EmailField(blank=False, null=False)
    direccion = models.TextField(blank=False, null=False)
    metpago = models.CharField(max_length=45, blank=False, null=False)
    usuario_id = models.OneToOneField(Usuario, on_delete=models.CASCADE)
    def __str__(self):
        return '%s %s %s' % (self.nombre, self.apellido, self.email)


class Categoria(models.Model):
    nombre = models.CharField(max_length=45, blank=False, null=False)

    def __str__(self):
        return self.nombre

class Producto(models.Model):

    nombre = models.CharField(max_length=45, blank=False, null=False)
    precio = models.FloatField(blank=False, null=False)
    cantidad = models.IntegerField(blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    description = models.TextField(blank=False, null=False)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    image = models.ManyToManyField('Image')
    
    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return self.nombre

 
class Image(models.Model):
    name = models.CharField(
        max_length=255
    )
    image = VersatileImageField(
        'Image',
        upload_to='images/',
        ppoi_field='ppoi')
    ppoi = PPOIField()
    def __str__(self):
        return self.name

class Pedido(models.Model):
    fecha = models.DateTimeField(auto_now_add=True)
    descuento = models.FloatField(blank=True, null=True)
    descripcion = models.TextField(blank=True, null=True)
    iva = models.FloatField(blank=False, null=False)
    producto_as = models.ManyToManyField('Producto')
    cliente_id = models.OneToOneField(Cliente, on_delete=models.CASCADE)


class Venta(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    total = models.FloatField(blank=False, null=False)
    Pedido_id = models.OneToOneField(Pedido, on_delete=models.CASCADE)
    Pedido_Cliente_id = models.OneToOneField(Cliente, on_delete=models.CASCADE)
