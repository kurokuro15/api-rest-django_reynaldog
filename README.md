Proyecto final de Algoritmo y programación II 

Desarrollado por Reynaldo González

Utilizando las tecnologías de Angular para el frontend y Django Api Rest para el backend junto a una base de datos SQLite3. 


Dependencias y versiones utilizadas:

Frontend: 

    Dependencias:
    
        //Se pueden instalar solo con npm install 
        
        Angular 11.2.11
        FontAwesome( NgFontAwesome) 0.8.2
        Bootstrap ( NgBootstrap) 4.6.0
        jquery 3.6.0
        rxjs 6.6.7
        typescript 4.1.5

        //Se debe arrancar en el purto 4200 de localhost. 


Backend:

    Librerías: 
        appdirs==1.4.4
        asgiref==3.3.4
        black==21.6b0
        click==8.0.1
        colorama==0.4.4
        Django==3.2.4
        django-cors-headers==3.7.0
        django-filter==2.4.0
        django-versatileimagefield==2.1
        djangorestframework==3.12.4
        drf-flex-fields==0.9.1
        mypy-extensions==0.4.3
        pathspec==0.8.1
        Pillow==8.2.0
        python-magic==0.4.24
        python-magic-bin==0.4.14
        pytz==2021.1
        regex==2021.4.4
        sqlparse==0.4.1
        toml==0.10.2
    //Se debe arrancar en el puerto 8000 de localhost


Puesta en marcha:

    Para el servidor de frontend basta con realizar los siguientes comandos desde la ruta ../KuroNoDagashi/

    -   'npm install' 
    -   'ng serve --open' <Puede estar en el puerto 4200 u otro diferente>

    Para el servidor de backend se debería de crear un entorno virtual utilizando virtual env en la raíz del proyecto sería: 

    ../Git_carpeta/
    -   'virtualenv kuronodagashi'
    -   '.\scripts\activate' 
    Desde aquí se realizaría la instalación de las dependencias mencionadas arriba con:
    -   'pip install <dependencia>'
    Posterior a esto debería estar todo listo para arrancar el servidor desde la ruta : '../Git_carpeta/src/'
    -   'python manage.py runserver' <debe arrancar en el puerto 8000 de localhost>

Con esto debería de correr correctamente el servidor, se adjunta al GIT la base de datos <<db.sqlite3>> donde se encuentran productos de ejemplo (poco configurados) para la muestra del funcionamiento de la página y del consumo de la API Rest por parte de Angular. 
